use std::{fmt::Display, time::Duration};

pub const MAX_SIMULATION_STEPS: u64 = 1_000_000;
pub const MAX_OUTPUT_STEPS: u64 = 1_000_000;
pub const MIN_TIME_STEP: Duration = Duration::from_micros(1);
pub const MAX_VARIABLES: usize = 32;
pub const MAX_VARIABLE_VALUE: f64 = 1e30;

#[derive(Clone, PartialEq, Debug)]
pub enum VariableDefinitionErrorKind {
    UnavailableName,
    InvalidName,
    DuplicateName,
    InvalidExpression(meval::Error),
}

#[derive(Clone, PartialEq, Debug)]
pub struct VariableDefinitionError {
    pub name: String,
    pub kind: VariableDefinitionErrorKind,
}

impl Display for VariableDefinitionError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match &self.kind {
            VariableDefinitionErrorKind::UnavailableName => {
                format!("Name {} is not available", self.name)
            }
            VariableDefinitionErrorKind::InvalidName => format!("Name {} is invalid", self.name),
            VariableDefinitionErrorKind::DuplicateName => {
                format!("Multiple variables have the same name {}", self.name)
            }
            VariableDefinitionErrorKind::InvalidExpression(err) => {
                format!("Variable {} expression is invalid: {}", self.name, err)
            }
        };
        f.write_str(&s)?;
        Ok(())
    }
}

#[derive(Clone, PartialEq, Debug)]
pub enum SystemDefinitionError {
    TooManyVariables,
    InvalidVariableDefinitions(Vec<VariableDefinitionError>),
}

impl Display for SystemDefinitionError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match &self {
            SystemDefinitionError::TooManyVariables => {
                format!("Exceeded maximum number of variables ({MAX_VARIABLES})")
            }
            SystemDefinitionError::InvalidVariableDefinitions(errors) => match errors.len() {
                1 => "1 variable definition error found".to_owned(),
                count => format!("{count} variable definition errors found"),
            },
        };
        f.write_str(&s)?;
        Ok(())
    }
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub enum SimulationParametersError {
    StartAfterEnd,
    TooManySimulationSteps,
    TooManyOutputSteps,
    TimeStepTooSmall,
}

impl Display for SimulationParametersError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            SimulationParametersError::StartAfterEnd => "Start time is after end time".to_string(),
            SimulationParametersError::TooManySimulationSteps => {
                format!("Number of simulation steps exceed maximum ({MAX_SIMULATION_STEPS})")
            }
            SimulationParametersError::TooManyOutputSteps => {
                format!("Number of output steps exceeds maximum ({MAX_OUTPUT_STEPS})")
            }
            SimulationParametersError::TimeStepTooSmall => format!(
                "Time step is less than the minimum {} µs",
                MIN_TIME_STEP.as_micros()
            ),
        };
        f.write_str(&s)?;
        Ok(())
    }
}

#[derive(Clone, PartialEq, Debug)]
pub enum SimulationError {
    SystemDefinitionError(SystemDefinitionError),
    SimulationParametersError(SimulationParametersError),
    VariableValueTooLarge,
}

impl Display for SimulationError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            SimulationError::SystemDefinitionError(err) => {
                format!("Invalid system definition: {err}")
            }
            SimulationError::SimulationParametersError(err) => {
                format!("Invalid simulation parameters: {err}")
            }
            SimulationError::VariableValueTooLarge => format!(
                "A variable exceeded the maximum absolute value of {MAX_VARIABLE_VALUE:e} during the simulation"
            ),
        };
        f.write_str(&s)?;
        Ok(())
    }
}

impl From<meval::Error> for VariableDefinitionErrorKind {
    fn from(err: meval::Error) -> Self {
        VariableDefinitionErrorKind::InvalidExpression(err)
    }
}

impl From<(String, VariableDefinitionErrorKind)> for VariableDefinitionError {
    fn from(t: (String, VariableDefinitionErrorKind)) -> Self {
        let (name, kind) = t;
        Self { name, kind }
    }
}

impl From<SimulationParametersError> for SimulationError {
    fn from(err: SimulationParametersError) -> Self {
        SimulationError::SimulationParametersError(err)
    }
}

impl From<SystemDefinitionError> for SimulationError {
    fn from(err: SystemDefinitionError) -> Self {
        SimulationError::SystemDefinitionError(err)
    }
}
