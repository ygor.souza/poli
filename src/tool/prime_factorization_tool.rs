use std::collections::HashMap;
use std::time::Instant;

use super::Tool;
use eframe::egui::{self, Layout, RichText};
use eframe::emath::Align;

pub struct PrimeFactorization {
    number: u64,
    factors: Vec<(u64, u8)>,
}

impl Default for PrimeFactorization {
    fn default() -> Self {
        Self {
            number: 123456789,
            factors: vec![],
        }
    }
}

impl PrimeFactorization {
    fn update_factors(&mut self) {
        if get_number(self.factors.iter().map(|(f, e)| (f, e))) != self.number {
            let start = Instant::now();
            self.factors = get_prime_factors(self.number)
                .into_iter()
                .collect::<Vec<(_, _)>>();
            let elapsed = start.elapsed();
            log::trace!("Found prime factors in {elapsed:#?}");
            self.factors.sort_by_key(|t| t.0);
        }
    }
}

impl Tool for PrimeFactorization {
    fn view(&mut self, ui: &mut egui::Ui) {
        self.update_factors();
        ui.heading(self.name());
        let initial_size = [ui.available_width(), ui.spacing().interact_size.y].into();
        let layout = Layout::left_to_right(Align::BOTTOM).with_main_wrap(true);
        ui.allocate_ui_with_layout(initial_size, layout, |ui| {
            ui.spacing_mut().item_spacing.x = 0.0;
            ui.add(egui::DragValue::new(&mut self.number).clamp_range(2..=u64::MAX));
            ui.label(" = ");
            let mut first = true;
            self.factors.iter().for_each(|(factor, exp)| {
                if !first {
                    ui.label(" × ");
                } else {
                    first = false;
                }
                ui.label(factor.to_string());
                if exp > &1 {
                    ui.label(RichText::new(exp.to_string()).small_raised());
                }
            })
        });
    }

    fn name(&self) -> &str {
        "Prime Factors"
    }
}

fn get_prime_factors(n: u64) -> HashMap<u64, u8> {
    if n < 2 {
        return HashMap::default();
    }
    let candidates = [2, 3]
        .into_iter()
        .chain((1..n).flat_map(|i| {
            let i = 6 * i;
            [i - 1, i + 1]
        }))
        .take_while(|f| f * f <= n);
    for factor in candidates {
        if n % factor == 0 {
            let mut map = get_prime_factors(n / factor);
            map.entry(factor)
                .and_modify(|count| *count += 1)
                .or_insert(1);
            return map;
        }
    }
    HashMap::from([(n, 1)])
}

fn get_number<'a, I>(prime_factors: I) -> u64
where
    I: IntoIterator<Item = (&'a u64, &'a u8)>,
{
    prime_factors
        .into_iter()
        .map(|(factor, exp)| factor.pow(*exp as u32))
        .product()
}

#[cfg(test)]
mod tests {
    use super::*;
    const PRIMES_UNTIL_30: [u64; 10] = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29];

    #[test]
    fn get_prime_factors_works() {
        assert_eq!(get_prime_factors(29), HashMap::from([(29u64, 1u8)]));
        assert_eq!(
            get_prime_factors(2u64.pow(2) * 5u64.pow(1)),
            HashMap::from([(2, 2), (5, 1)])
        );
        let factor_sets = vec![
            vec![],
            vec![1u8],
            vec![5],
            vec![0, 1],
            vec![1, 1],
            vec![0, 3, 2],
            vec![1, 0, 1],
            vec![1, 1, 1],
            vec![4, 0, 0, 3],
        ]
        .into_iter()
        .map(|vec| {
            PRIMES_UNTIL_30
                .into_iter()
                .zip(vec)
                .filter(|(_factor, exp)| exp > &0)
                .collect::<HashMap<_, _>>()
        });
        for set in factor_sets {
            let n = get_number(&set);
            assert_eq!(get_prime_factors(n), set);
        }
    }
}
