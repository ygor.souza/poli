use super::Tool;
use eframe::{
    egui::{self, DragValue, Label, Layout, RichText},
    epaint::Color32,
};
use egui_extras::{Column, TableBuilder};
use egui_plot::{Legend, Line, Plot, PlotPoints};

pub struct ScalarFunctionPlotter {
    variable: String,
    function: String,
    points: Vec<f64>,
}

impl Default for ScalarFunctionPlotter {
    fn default() -> Self {
        Self {
            variable: "x".to_owned(),
            function: "sin(x)/x + tanh(x)^2 + ln((x + 5)^3)".to_owned(),
            points: vec![1.0, 2.0, 3.0],
        }
    }
}

impl ScalarFunctionPlotter {
    fn get_function(&mut self, ui: &mut egui::Ui) {
        ui.horizontal(|ui| {
            ui.horizontal(|ui| {
                ui.spacing_mut().item_spacing = [0.0, 0.0].into();
                ui.spacing_mut().text_edit_width = 15.0;
                ui.label("f(");
                ui.text_edit_singleline(&mut self.variable);
                ui.label(") = ");
            });
            ui.text_edit_singleline(&mut self.function);
        });
    }

    fn add_points(&mut self, ui: &mut egui::Ui) {
        ui.horizontal(|ui| {
            ui.label("Points");
            if ui.button("➕").clicked() {
                let step = self
                    .points
                    .last()
                    .map(|last| {
                        last - self
                            .points
                            .get(self.points.len().saturating_sub(2))
                            .unwrap_or(&0.0)
                    })
                    .unwrap_or(1.0);
                self.points.push(self.points.last().unwrap_or(&0.0) + step);
            }
        });
    }

    fn plot_function<F: Fn(f64) -> f64 + 'static>(&mut self, func: F, ui: &mut egui::Ui) {
        let height = ui.available_height();
        ui.horizontal(|ui| {
            ui.allocate_ui_with_layout(
                [ui.available_width(), height].into(),
                Layout::default(),
                |ui| {
                    self.add_points(ui);

                    let table = TableBuilder::new(ui)
                        .cell_layout(egui::Layout::left_to_right(egui::Align::Center))
                        .column(Column::initial(80.0))
                        .column(Column::initial(80.0))
                        .column(Column::initial(20.0))
                        .resizable(false);

                    table.body(|mut body| {
                        self.points = self
                            .points
                            .iter()
                            .cloned()
                            .filter_map(|mut x| {
                                let y = func(x);
                                let mut new_x = Some(x);
                                body.row(20.0, |mut row| {
                                    row.col(|ui| {
                                        ui.label("f(");
                                        ui.add(DragValue::new(&mut x));
                                        ui.label(") = ");
                                    });
                                    row.col(|ui| {
                                        ui.label(format!("{y:.6e}"));
                                    });
                                    row.col(|ui| {
                                        new_x = if ui.button("❌").clicked() {
                                            None
                                        } else {
                                            Some(x)
                                        }
                                    });
                                });
                                new_x
                            })
                            .collect();
                    });
                },
            );
            let line =
                Line::new(PlotPoints::from_explicit_callback(func, .., 512)).name(&self.function);
            let plot = Plot::new("function_plot").legend(Legend::default());
            ui.allocate_ui_with_layout(
                [ui.available_width(), height].into(),
                Layout::default(),
                |ui| {
                    let response = plot.show(ui, |plot_ui| {
                        plot_ui.line(line);
                        plot_ui.pointer_coordinate()
                    });
                    if response
                        .response
                        .double_clicked_by(egui::PointerButton::Primary)
                    {
                        if let Some(point) = response.inner {
                            self.points.push(point.x);
                        }
                    }
                },
            );
        });
    }
}

impl Tool for ScalarFunctionPlotter {
    fn view(&mut self, ui: &mut egui::Ui) {
        ui.heading("Scalar function plotter");
        self.get_function(ui);
        let func = self
            .function
            .parse::<meval::Expr>()
            .and_then(|expr| expr.bind(&self.variable));

        match func {
            Ok(func) => self.plot_function(func, ui),
            Err(err) => {
                ui.add(Label::new(
                    RichText::new(format!("{err:?}")).color(Color32::RED),
                ));
            }
        }
    }

    fn name(&self) -> &str {
        "Function Graph"
    }
}
