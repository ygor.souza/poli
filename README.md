# Poli

GUI application containing useful tools for STEM, written in the Rust
programming language.

Currently has the following tools available:
- A widget that converts back and forth between a real number and its 32-bit
  floating point representation as specified by IEEE 754-2008.
- A set of unit converters for various quantities such as length, force, energy
  etc.
- A periodic table that displays information on each element on hover.
- A widget that calculates the prime factors of a number.
- A widget that parses a scalar function from a string and plots its graph.
- A widget that allows defining a dynamical system and simulating its evolution
  over time.

## Using this application

Binary executables are not provided for the moment, so the application must be
compiled from source:

- Install the Rust compiler and tools following the instructions in
  https://www.rust-lang.org/tools/install
- Open a command prompt in the project's folder and run the command
`cargo build --release`. This will create an executable file in the
`target/release/` subdirectory, which can be copied to another folder and used
to launch the application.
