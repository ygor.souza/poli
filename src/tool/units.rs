use std::fmt::Display;
use std::marker::PhantomData;
use std::str::FromStr;

use egui_extras::Column;
use egui_extras::TableBody;
use uom::fmt::DisplayStyle::Abbreviation;
use uom::si::f64::Energy;
use uom::si::f64::Force;
use uom::si::f64::Length;
use uom::si::f64::Mass;
use uom::si::f64::Power;
use uom::si::f64::Pressure;
use uom::si::f64::ThermalConductivity;
use uom::si::f64::ThermodynamicTemperature;
use uom::si::f64::Volume;
use uom::si::f64::VolumeRate;

use uom::si::energy::{self, joule};
use uom::si::force::{self, newton};
use uom::si::length::{self, meter};
use uom::si::mass::{self, kilogram};
use uom::si::power::{self, watt};
use uom::si::pressure::{self, pascal};
use uom::si::thermal_conductivity::{self, watt_per_meter_kelvin};
use uom::si::thermodynamic_temperature::{self, kelvin};
use uom::si::volume::{self, cubic_meter};
use uom::si::volume_rate::{self, cubic_meter_per_second};

use enum_iterator::all;
use enum_iterator::Sequence;

use super::Tool;
use eframe::egui::{self, ComboBox};

use egui_extras::TableBuilder;

pub struct Units {
    last_id: u64,
    converters: Vec<Box<dyn AnyConverter>>,
    quantity: QuantityOption,
}

impl Units {
    pub fn new() -> Self {
        Self {
            last_id: 0,
            converters: Vec::new(),
            quantity: QuantityOption::Length,
        }
    }
}
impl Default for Units {
    fn default() -> Self {
        Self::new()
    }
}

impl Tool for Units {
    fn name(&self) -> &str {
        "Units"
    }

    fn view(&mut self, ui: &mut egui::Ui) {
        ui.heading("Unit conversion");
        ui.vertical(|ui| {
            ui.horizontal(|ui| {
                ComboBox::from_id_source("quantity")
                    .selected_text(self.quantity.to_string())
                    .show_ui(ui, |ui| {
                        for quantity in all::<QuantityOption>() {
                            ui.selectable_value(&mut self.quantity, quantity, quantity.to_string());
                        }
                    });
                if ui.button("➕").clicked() && self.converters.len() < 20 {
                    self.converters
                        .push(self.quantity.make_converter(self.last_id));
                    self.last_id += 1;
                }
            });
            let table = TableBuilder::new(ui)
                .cell_layout(egui::Layout::left_to_right(egui::Align::LEFT))
                .column(Column::initial(60.0))
                .column(Column::initial(120.0))
                .column(Column::initial(120.0))
                .column(Column::initial(20.0))
                .resizable(false);
            table.body(|mut body| {
                self.converters
                    .retain_mut(|c| !c.show_and_check_if_deleted(&mut body));
            });
        });
    }
}

#[derive(PartialEq, Eq, Clone, Copy, Sequence)]
pub enum QuantityOption {
    Energy,
    Force,
    Length,
    Mass,
    Power,
    Pressure,
    ThermalConductivity,
    ThermodynamicTemperature,
    Volume,
    VolumeRate,
}

impl Display for QuantityOption {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            QuantityOption::Energy => energy::description(),
            QuantityOption::Force => force::description(),
            QuantityOption::Length => length::description(),
            QuantityOption::Mass => mass::description(),
            QuantityOption::Power => power::description(),
            QuantityOption::Pressure => pressure::description(),
            QuantityOption::ThermalConductivity => thermal_conductivity::description(),
            QuantityOption::ThermodynamicTemperature => thermodynamic_temperature::description(),
            QuantityOption::Volume => volume::description(),
            QuantityOption::VolumeRate => volume_rate::description(),
        };
        f.write_str(s)?;
        Ok(())
    }
}

impl QuantityOption {
    fn make_converter(&self, id: u64) -> Box<dyn AnyConverter> {
        match self {
            QuantityOption::Energy => Box::new(Converter::<Energy, energy::Units>::new(id)),
            QuantityOption::Force => Box::new(Converter::<Force, force::Units>::new(id)),
            QuantityOption::Length => Box::new(Converter::<Length, length::Units>::new(id)),
            QuantityOption::Mass => Box::new(Converter::<Mass, mass::Units>::new(id)),
            QuantityOption::Power => Box::new(Converter::<Power, power::Units>::new(id)),
            QuantityOption::Pressure => Box::new(Converter::<Pressure, pressure::Units>::new(id)),
            QuantityOption::ThermalConductivity => {
                Box::new(Converter::<ThermalConductivity, thermal_conductivity::Units>::new(id))
            }
            QuantityOption::ThermodynamicTemperature => Box::new(Converter::<
                ThermodynamicTemperature,
                thermodynamic_temperature::Units,
            >::new(id)),
            QuantityOption::Volume => Box::new(Converter::<Volume, volume::Units>::new(id)),
            QuantityOption::VolumeRate => {
                Box::new(Converter::<VolumeRate, volume_rate::Units>::new(id))
            }
        }
    }
}

#[allow(dead_code)]
pub trait AnyUnit: Copy {
    fn abbreviation(&self) -> &'static str;
    fn singular(&self) -> &'static str;
    fn plural(&self) -> &'static str;
    fn all_values() -> Box<dyn Iterator<Item = Self>>;
}

#[allow(dead_code)]
pub trait AnyQuantity: FromStr {
    fn description() -> &'static str;
    fn format_as_si_unit(&self) -> String;
}

macro_rules! quantity_option {
    ($mod:ident, $quantity:ident, $unit:ident) => {
        impl AnyUnit for uom::si::$mod::Units {
            fn abbreviation(&self) -> &'static str {
                self.abbreviation()
            }

            fn singular(&self) -> &'static str {
                self.singular()
            }

            fn plural(&self) -> &'static str {
                self.plural()
            }

            fn all_values() -> Box<dyn Iterator<Item = Self>> {
                Box::new(uom::si::$mod::units())
            }
        }

        impl AnyQuantity for uom::si::f64::$quantity {
            fn description() -> &'static str {
                $mod::description()
            }

            fn format_as_si_unit(&self) -> String {
                let m = Self::format_args($unit, Abbreviation);
                format!("{:e}", m.with(*self))
            }
        }
    };
}

quantity_option!(energy, Energy, joule);
quantity_option!(force, Force, newton);
quantity_option!(length, Length, meter);
quantity_option!(mass, Mass, kilogram);
quantity_option!(power, Power, watt);
quantity_option!(pressure, Pressure, pascal);
quantity_option!(
    thermal_conductivity,
    ThermalConductivity,
    watt_per_meter_kelvin
);
quantity_option!(thermodynamic_temperature, ThermodynamicTemperature, kelvin);
quantity_option!(volume, Volume, cubic_meter);
quantity_option!(volume_rate, VolumeRate, cubic_meter_per_second);

pub trait AnyConverter {
    fn show_and_check_if_deleted(&mut self, ui: &mut TableBody) -> bool;
}

pub struct Converter<V, U> {
    id: u64,
    value: f64,
    unit1: U,
    quantity: PhantomData<V>,
}

impl<V, U> Converter<V, U>
where
    U: AnyUnit,
{
    fn new(id: u64) -> Self {
        Self {
            id,
            value: 1.0,
            unit1: U::all_values().next().unwrap(),
            quantity: PhantomData,
        }
    }
}

impl<V, U> AnyConverter for Converter<V, U>
where
    V: AnyQuantity,
    U: AnyUnit,
{
    fn show_and_check_if_deleted(&mut self, body: &mut TableBody) -> bool {
        let mut deleted = false;
        body.row(20.0, |mut row| {
            row.col(|ui| {
                ui.add(egui::DragValue::new(&mut self.value));
            });
            row.col(|ui| {
                ComboBox::from_id_source(self.id)
                    .selected_text(self.unit1.abbreviation())
                    .show_ui(ui, |ui| {
                        let mut abr = self.unit1.abbreviation();
                        for unit in U::all_values() {
                            ui.selectable_value(
                                &mut abr,
                                unit.abbreviation(),
                                format!("{} ({})", unit.abbreviation(), unit.singular()),
                            );
                        }
                        if self.unit1.abbreviation() != abr {
                            self.unit1 = U::all_values()
                                .find(|u| u.abbreviation() == abr)
                                .unwrap_or(self.unit1);
                        }
                    });
            });
            row.col(|ui| {
                if let Ok(q) = format!("{} {}", self.value, self.unit1.abbreviation()).parse::<V>()
                {
                    ui.horizontal(|ui| {
                        ui.heading("=");
                        ui.label(q.format_as_si_unit());
                    });
                }
            });
            row.col(|ui| {
                deleted = ui.button("❌").clicked();
            });
        });
        deleted
    }
}
