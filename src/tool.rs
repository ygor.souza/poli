use eframe::egui;

pub mod dynamical_system_simulator;
pub mod float;
pub mod periodic_table_tool;
pub mod prime_factorization_tool;
pub mod scalar_function_plotter;
pub mod units;

pub trait Tool {
    fn name(&self) -> &str;

    fn view(&mut self, ui: &mut egui::Ui);
}

pub fn all_tools() -> Vec<Box<dyn Tool>> {
    vec![
        Box::<units::Units>::default(),
        Box::<float::Float>::default(),
        Box::<periodic_table_tool::PeriodicTableTool>::default(),
        Box::<prime_factorization_tool::PrimeFactorization>::default(),
        Box::<scalar_function_plotter::ScalarFunctionPlotter>::default(),
        Box::<dynamical_system_simulator::SimulationTool>::default(),
    ]
}
