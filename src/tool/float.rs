use std::{fmt::Display, num::ParseIntError};

use super::Tool;
use eframe::egui::{self, ComboBox, TextEdit};

#[derive(PartialEq, Clone, Copy)]
enum Radix {
    Decimal,
    Binary,
    Octal,
    Hexadecimal,
}

impl Radix {
    fn all_variants() -> [Self; 4] {
        [Self::Decimal, Self::Binary, Self::Octal, Self::Hexadecimal]
    }
    fn width(&self) -> usize {
        match self {
            Radix::Decimal => 3,
            Radix::Binary => 8,
            Radix::Octal => 3,
            Radix::Hexadecimal => 2,
        }
    }
    fn format(&self, byte: &u8) -> String {
        let w = self.width();
        match self {
            Radix::Decimal => format!("{byte:0w$}"),
            Radix::Binary => format!("{byte:0w$b}"),
            Radix::Octal => format!("{byte:0w$o}"),
            Radix::Hexadecimal => format!("{byte:0w$X}"),
        }
    }

    fn parse(&self, s: &str) -> Result<u8, ParseIntError> {
        let radix = match self {
            Radix::Decimal => 10,
            Radix::Binary => 2,
            Radix::Octal => 8,
            Radix::Hexadecimal => 16,
        };
        u8::from_str_radix(s, radix)
    }
}

impl Display for Radix {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            Radix::Decimal => "Dec",
            Radix::Binary => "Bin",
            Radix::Octal => "Oct",
            Radix::Hexadecimal => "Hex",
        };
        f.write_str(s)?;
        Ok(())
    }
}

#[derive(PartialEq, Clone, Copy)]
enum Endianness {
    Big,
    Little,
    Native,
}

impl Endianness {
    fn all_variants() -> [Self; 3] {
        [Self::Big, Self::Little, Self::Native]
    }
    fn get_bytes(&self, x: f32) -> [u8; 4] {
        match self {
            Endianness::Big => x.to_be_bytes(),
            Endianness::Little => x.to_le_bytes(),
            Endianness::Native => x.to_ne_bytes(),
        }
    }
    fn get_value(&self, bytes: [u8; 4]) -> f32 {
        match self {
            Endianness::Big => f32::from_be_bytes(bytes),
            Endianness::Little => f32::from_le_bytes(bytes),
            Endianness::Native => f32::from_ne_bytes(bytes),
        }
    }
}

impl Display for Endianness {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            Endianness::Big => "Big Endian",
            Endianness::Little => "Little Endian",
            Endianness::Native => "Native Endianness",
        };
        f.write_str(s)?;
        Ok(())
    }
}

pub struct Float {
    float: f32,
    radix: Radix,
    endianness: Endianness,
}

impl Default for Float {
    fn default() -> Self {
        Self {
            float: 12.5f32,
            radix: Radix::Hexadecimal,
            endianness: Endianness::Big,
        }
    }
}

impl Tool for Float {
    fn view(&mut self, ui: &mut egui::Ui) {
        ui.heading("IEEE 754-2008 floating point viewer");
        ui.horizontal(|ui| {
            ui.add(egui::DragValue::new(&mut self.float));
            ui.label("Floating point value");
        });
        ui.horizontal(|ui| {
            let mut int = self.float.to_bits();
            if ui.add(egui::DragValue::new(&mut int)).changed() {
                self.float = f32::from_bits(int);
            }
            ui.label("Integer from bits");
        });
        ui.horizontal(|ui| {
            ComboBox::from_label("")
                .selected_text(self.radix.to_string())
                .show_ui(ui, |ui| {
                    Radix::all_variants()
                        .map(|r| ui.selectable_value(&mut self.radix, r, r.to_string()));
                });
            ComboBox::from_id_source("endian")
                .selected_text(self.endianness.to_string())
                .show_ui(ui, |ui| {
                    Endianness::all_variants()
                        .map(|e| ui.selectable_value(&mut self.endianness, e, e.to_string()));
                });
        });
        ui.horizontal(|ui| {
            let mut bytes = self.endianness.get_bytes(self.float);
            if bytes
                .iter_mut()
                .map(|b| {
                    let mut s = self.radix.format(b);
                    let r = TextEdit::singleline(&mut s)
                        .desired_width(8.0 * self.radix.width() as f32)
                        .show(ui)
                        .response;
                    if r.changed() {
                        let n = self.radix.width().min(s.len());
                        *b = self.radix.parse(&s[..n]).unwrap_or(*b);
                    }
                    r
                })
                .fold(false, |t, r| t | r.changed())
            {
                self.float = self.endianness.get_value(bytes);
            }
        });
    }

    fn name(&self) -> &str {
        "Float"
    }
}
