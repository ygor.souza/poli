mod errors;
mod system;

use std::{
    error::Error,
    fs::File,
    io::{Read, Write},
    path::PathBuf,
    rc::Rc,
    time::{Duration, Instant},
};

use self::{
    errors::{SimulationError, SystemDefinitionError},
    system::{
        CustomVariable, DynamicalSystem, SimulationDefinition, SimulationParameters,
        SimulationResults,
    },
};

use super::Tool;

use chrono::prelude::*;
use eframe::{
    egui::{self, DragValue, Label, Layout, RichText, ScrollArea},
    epaint::Color32,
};

use egui_extras::{Column, TableBuilder};
use egui_plot::{Legend, Line, Plot, PlotPoints};
use log::{error, warn};

pub struct SimulationTool {
    parameters: SimulationParameters,
    system: DynamicalSystem,
    parameters_changed: bool,
    live_update: bool,
    results: Option<Result<Rc<SimulationResults>, SimulationError>>,
    export_error: Result<(), Box<dyn Error>>,
    save_load_error: Result<(), Box<dyn Error>>,
}

impl Default for SimulationTool {
    fn default() -> Self {
        Self {
            parameters: SimulationParameters::default(),
            system: DynamicalSystem::default(),
            parameters_changed: true,
            live_update: true,
            results: None,
            export_error: Ok(()),
            save_load_error: Ok(()),
        }
    }
}

impl SimulationTool {
    fn edit_parameters(&mut self, ui: &mut egui::Ui) {
        ui.horizontal(|ui| {
            ui.label("Simulation parameters");
            if let Err(err) = self.parameters.validate() {
                ui.add(Label::new(RichText::new("⚠").color(Color32::RED)))
                    .on_hover_ui(|ui| {
                        ui.add(Label::new(
                            RichText::new(err.to_string()).color(Color32::RED),
                        ));
                    });
            }
        });
        self.show_load_save_buttons(ui);
        ui.horizontal(|ui| {
            ui.label("Start time (s)");
            let mut time = self.parameters.start_time_mut().as_secs_f64();
            self.parameters_changed |= ui
                .add(DragValue::new(&mut time))
                .on_hover_ui(|ui| {
                    ui.label("Time after which the simulation starts recording the results");
                    ui.label("It always simulates from 0 to End time, but only records from Start time to End time");
                })
                .changed();
            *self.parameters.start_time_mut() = Duration::from_secs_f64(time.max(0.0));
        });
        ui.horizontal(|ui| {
            ui.label("End time (s)");
            let mut time = self.parameters.end_time_mut().as_secs_f64();
            self.parameters_changed |= ui.add(DragValue::new(&mut time)).changed();
            *self.parameters.end_time_mut() = Duration::from_secs_f64(time.max(0.0));
        });
        ui.horizontal(|ui| {
            ui.label("Time step (s)");
            let mut time = self.parameters.time_step_mut().as_secs_f64();
            self.parameters_changed |= ui
                .add(
                    DragValue::new(&mut time)
                        .fixed_decimals(6)
                        .clamp_range(1e-6..=1.0),
                )
                .on_hover_ui(|ui| {
                    ui.label("Time between two consecutive steps in the simulation");
                    ui.label("Available via the \"dt\" predefined variable");
                })
                .changed();
            *self.parameters.time_step_mut() = Duration::from_secs_f64(time.max(1e-6));
        });
        ui.horizontal(|ui| {
            ui.label("Noise seed");
            self.parameters_changed |= ui
                .add(DragValue::new(self.parameters.noise_seed_mut()))
                .on_hover_ui(|ui| {
                    ui.label("Seed to change the random noise generated for the simulation");
                    ui.label("Available via the \"noise\" predefined variable");
                })
                .changed();
        });
        let run = ui
            .horizontal(|ui| {
                let can_run = self.parameters.validate().is_ok() && self.system.validate().is_ok();
                ui.checkbox(&mut self.live_update, "Live Update")
                    .on_hover_text(
                        "Rerun simulation automatically when parameters or variables are changed",
                    );
                let run = can_run && {
                    let manual_run = ui.button("Run").clicked();
                    (self.live_update && self.parameters_changed) || manual_run
                };

                if let Some(Ok(results)) = &self.results {
                    if ui
                        .button("Export")
                        .on_hover_text("Export the current simulation results to CSV")
                        .clicked()
                    {
                        let default_file_name = Local::now()
                            .format("poli_simulation_%Y%m%d_%H%M%S.csv")
                            .to_string();
                        if let Some(path) = rfd::FileDialog::new()
                            .set_file_name(default_file_name)
                            .save_file()
                        {
                            self.export_error = self.export_results(results, path);
                        }
                    }
                }
                if let Some(Err(err)) = &self.results {
                    ui.button("⚠").on_hover_ui(|ui| {
                        ui.add(Label::new(
                            RichText::new(err.to_string()).color(Color32::RED),
                        ));
                    });
                }
                if let Err(err) = &self.export_error {
                    let clear_error = ui
                        .button("⚠")
                        .on_hover_ui(|ui| {
                            ui.add(Label::new(
                                RichText::new(err.to_string()).color(Color32::RED),
                            ));
                        })
                        .clicked();
                    if clear_error {
                        self.export_error = Ok(());
                    };
                }
                run
            })
            .inner;
        if run {
            let start = Instant::now();
            let results = self.system.run_simulation(&self.parameters).map(Rc::new);
            let elapsed = start.elapsed();
            match results.as_ref() {
                Ok(_) => log::trace!("Simulated system in {elapsed:#?}"),
                Err(err) => log::error!("Error simulating system: {err}"),
            }
            self.results = Some(results);
            self.parameters_changed = false;
        }
    }

    fn show_load_save_buttons(&mut self, ui: &mut egui::Ui) {
        ui.horizontal(|ui| {
            if ui
                .button("Load")
                .on_hover_text("Load system definition from a file")
                .clicked()
            {
                if let Some(path) = rfd::FileDialog::new()
                    .add_filter("JSON files", &["json"])
                    .pick_file()
                {
                    self.save_load_error = self.load_simulation_definition(path).map_err(|err| {
                        error!("{err}");
                        err
                    });
                }
            }

            if ui
                .button("Save")
                .on_hover_text("Save system definition to a file")
                .clicked()
            {
                let default_file_name = Local::now()
                    .format("poli_dyn_system_%Y%m%d_%H%M%S.json")
                    .to_string();
                if let Some(path) = rfd::FileDialog::new()
                    .add_filter("JSON files", &["json"])
                    .set_file_name(default_file_name)
                    .save_file()
                {
                    self.save_load_error = self.save_simulation_definition(path);
                }
            }

            if let Err(err) = &self.save_load_error {
                let clear_error = ui
                    .button("⚠")
                    .on_hover_ui(|ui| {
                        ui.add(Label::new(
                            RichText::new(err.to_string()).color(Color32::RED),
                        ));
                    })
                    .clicked();
                if clear_error {
                    self.save_load_error = Ok(());
                };
            }
        });
    }

    fn edit_variables(&mut self, ui: &mut egui::Ui) {
        let system_definition_errors = self.system.validate();
        let (new_variable, variable_definition_errors) = ui
            .horizontal(|ui| {
                ui.label("Variables");
                let new_variable = ui
                    .button("➕")
                    .on_hover_text("Add a new variable")
                    .clicked();
                let variable_definition_errors = if let Err(err) = system_definition_errors {
                    ui.add(Label::new(RichText::new("⚠").color(Color32::RED)))
                        .on_hover_text(err.to_string());
                    match err {
                        SystemDefinitionError::TooManyVariables => None,
                        SystemDefinitionError::InvalidVariableDefinitions(errors) => Some(errors),
                    }
                } else {
                    None
                };
                (new_variable, variable_definition_errors)
            })
            .inner;

        let table = TableBuilder::new(ui)
            .cell_layout(egui::Layout::left_to_right(egui::Align::LEFT))
            .column(Column::initial(60.0))
            .column(Column::initial(40.0))
            .column(Column::initial(120.0))
            .column(Column::initial(10.0))
            .column(Column::initial(10.0))
            .column(Column::initial(20.0))
            .resizable(false);
        table.body(|mut body| {
        let mut variables: Vec<_> = self
            .system
            .variables()
            .iter()
            .cloned()
            .filter_map(|mut var| {
                let mut deleted = false;
                body.row(20.0, |mut row| {
                    row.col(|ui| {
                        self.parameters_changed |=
                            ui.text_edit_singleline(var.name_mut())
                            .on_hover_text("Variable name. Must start with a letter and contain only letters, numbers, and the '_' character")
                            .changed();
                    });
                    row.col(|ui| {
                        self.parameters_changed |= ui
                            .add(DragValue::new(var.initial_value_mut()).speed(0.1))
                            .on_hover_text("Initial value for this variable")
                            .changed();
                    });
                    row.col(|ui| {
                        let mut rule = var.update_rule().unwrap_or(&"".to_owned()).clone();
                        self.parameters_changed |= ui.text_edit_singleline(&mut rule)
                            .changed();
                        var.set_update_rule(Some(&rule));
                    });
                    row.col(|ui| {
                        ui.label("?")
                            .on_hover_ui( |ui| {
                                ui.label("Rule to update this variable on each step");
                                ui.label("as a function of the values of the variables in the previous step");
                                ui.label("The following predefined variables can also be used:");
                                ui.label("t: The current simulation time in seconds");
                                ui.label("dt: The simulation time step in seconds");
                                ui.label("noise: A pseudo-random value between -1 and 1, generated for each step");
                                ui.label("You can also use the variable name preceded by 'd' to get the difference");
                                ui.label("between its current value and its value in the previous step.");
                                ui.label("So, for example, 'dx/dt' approximates the derivative of x over time");
                                ui.label("Therefore, the name 'dx' cannot be used for another variable if there is");
                                ui.label("already a variable called 'x'");
                                ui.label("Supports the operators +, -, *, /, ^, %");
                                ui.label("as well as functions such as abs, round, ceil, floor, min, max");
                                ui.label("and trigonometric functions such as sin, cos, tan etc");
                                ui.label("The trigonometric functions assume that the value is in radians");
                            });
                    });
                    row.col(|ui| {
                        if variable_definition_errors
                            .iter()
                            .flatten()
                            .any(|err| err.name == var.name()) {
                            ui.add(Label::new(RichText::new("⚠").color(Color32::RED)))
                            .on_hover_ui(|ui| {
                                variable_definition_errors
                                    .iter()
                                    .flatten()
                                    .filter(|err| err.name == var.name())
                                    .for_each(|err| {
                                ui.label(err.to_string());
                                });
                            });
                        }
                    });
                    row.col(|ui| {
                        deleted = ui.button("❌").on_hover_text("Delete this variable").clicked();
                    });
                });
                self.parameters_changed |= deleted;
                match deleted {
                    true => None,
                    false => Some(var),
                }
            })
            .collect();

        if new_variable {
            if let Some(new_name) = (1..=variables.len() + 1)
                .map(|i| format!("x{i}"))
                .find(|name| !variables.iter().any(|var| var.name() == name))
            {
                variables.push(CustomVariable::new(&new_name, 0.0));
                self.parameters_changed = true;
            }
        }
        self.system.set_variables(variables);
            });
    }
    fn display_controls(&mut self, ui: &mut egui::Ui, height: f32) {
        ui.allocate_ui_with_layout(
            [ui.available_width(), height].into(),
            Layout::default(),
            |ui| {
                ScrollArea::vertical().show(ui, |ui| {
                    ui.group(|ui| {
                        self.edit_parameters(ui);
                    });
                    ui.group(|ui| {
                        self.edit_variables(ui);
                    });
                });
            },
        );
    }

    fn plot_results(&mut self, ui: &mut egui::Ui, height: f32) {
        if let Some(Ok(results)) = &self.results {
            let (time_index, start_time, end_time) = results
                .signals
                .iter()
                .enumerate()
                .find(|(_i, signal)| {
                    matches!(
                        signal.variable,
                        system::VariableKind::Predefined(system::PredefinedVariable::Time)
                    )
                })
                .map(|(i, signal)| {
                    (
                        i,
                        *signal.values.first().unwrap_or(&0.0),
                        *signal.values.last().unwrap_or(&1.0),
                    )
                })
                .unwrap_or_default();
            let plot = Plot::new("results").legend(Legend::default());
            ui.allocate_ui_with_layout(
                [ui.available_width(), height].into(),
                Layout::default(),
                |ui| {
                    plot.show(ui, |plot_ui| {
                        results
                            .signals
                            .iter()
                            .enumerate()
                            .filter(|(_, signal)| matches!(&signal.variable, system::VariableKind::Custom(var) if var.update_rule().is_some()))
                            .for_each(|(signal_index, signal)| {
                                let results = Rc::clone(results);
                                let try_get_y = move |t:f64| -> Option<f64> {
                                    let time = results.signals.get(time_index).map(|s| &s.values)?;
                                    let values = results.signals.get(signal_index).map(|s| &s.values)?;
                                    let i = time.partition_point(|t0| t0 < &t);
                                    let (t1, t0) = (time.get(i)?, time.get(i.checked_sub(1)?)?);
                                    let (v1, v0) = (values.get(i)?, values.get(i.checked_sub(1)?)?);
                                    Some(v0 + (v1 - v0)* (t - t0)/(t1 - t0))
                                    };
                                let get_y = move |t:f64| try_get_y(t).unwrap_or(f64::NAN);
                                let points = PlotPoints::from_explicit_callback(get_y, start_time..end_time, 4096);
                                let line = Line::new(points).name(signal.variable.name());
                                plot_ui.line(line);
                            });
                    });
                },
            );
        }
    }

    fn export_results(
        &self,
        results: &SimulationResults,
        path: PathBuf,
    ) -> Result<(), Box<dyn Error>> {
        let f = File::create(path)?;
        let mut wtr = csv::Writer::from_writer(f);
        wtr.write_record(results.signals.iter().map(|s| s.variable.name()))?;
        let n_rows = results.signals.first().map(|s| s.values.len()).unwrap_or(0);
        for row in 0..n_rows {
            wtr.write_record(results.signals.iter().map(|s| {
                s.values
                    .get(row)
                    .map(|v| v.to_string())
                    .unwrap_or_else(|| "".to_string())
            }))?;
        }
        Ok(())
    }

    fn load_old_format(&mut self, buf: &str) -> Result<(), Box<dyn Error>> {
        self.system = serde_json::from_str(buf)?;

        warn!("Loaded file in old format. Does not include simulation parameters");
        self.parameters_changed = true;
        Ok(())
    }

    fn load_simulation_definition(&mut self, path: PathBuf) -> Result<(), Box<dyn Error>> {
        let mut f = File::open(path)?;
        let mut buf = String::new();
        f.read_to_string(&mut buf)?;
        if let Ok(()) = self.load_old_format(&buf) {
            return Ok(());
        }
        let definition = serde_json::from_str(&buf)?;
        let SimulationDefinition { system, parameters } = definition;
        self.system = system;
        self.parameters = parameters;

        self.parameters_changed = true;
        Ok(())
    }

    fn save_simulation_definition(&self, path: PathBuf) -> Result<(), Box<dyn Error>> {
        let mut f = File::create(path)?;
        let definition = SimulationDefinition {
            system: self.system.clone(),
            parameters: self.parameters,
        };
        let buf = serde_json::to_string(&definition)?;
        f.write_all(buf.as_bytes())?;
        Ok(())
    }
}

impl Tool for SimulationTool {
    fn view(&mut self, ui: &mut egui::Ui) {
        ui.heading("Dynamical system simulator");
        let height = ui.available_height();
        ui.horizontal(|ui| {
            self.display_controls(ui, height);
            self.plot_results(ui, height);
        });
    }

    fn name(&self) -> &str {
        "System Simulation"
    }
}
