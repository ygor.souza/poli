use std::{collections::HashMap, fmt::Display, ops::RangeInclusive};

use eframe::{
    egui::{self, load::SizedTexture, ComboBox, TextureOptions, Ui},
    emath,
    epaint::{Color32, TextureHandle},
};

#[derive(PartialEq, Clone)]
pub enum ColormapType {
    Cpk,
    Jmol,
    AtomicNumber(RangeInclusive<u32>),
    AtomicRadius(RangeInclusive<f64>),
    AtomicWeight(RangeInclusive<f64>),
    Year(RangeInclusive<u16>),
}

impl Display for ColormapType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            ColormapType::Cpk => "CPK",
            ColormapType::Jmol => "Jmol",
            ColormapType::AtomicNumber(_) => "Atomic number",
            ColormapType::AtomicRadius(_) => "Atomic radius",
            ColormapType::AtomicWeight(_) => "Atomic weight",
            ColormapType::Year(_) => "Year discovered",
        };
        f.write_str(s)?;
        Ok(())
    }
}

impl ColormapType {
    pub fn get_element_color(
        &self,
        element: &mendeleev::Element,
        gradient: &colorgrad::Gradient,
    ) -> Option<Color32> {
        match &self {
            ColormapType::Cpk => cpk_to_egui_color(element.cpk_color()),
            ColormapType::Jmol => cpk_to_egui_color(element.jmol_color()),
            ColormapType::AtomicNumber(range) => get_atomic_number(element)
                .map(|n| lerp(n, range, 0.0..=1.0))
                .map(|n| get_color(gradient, n)),
            ColormapType::AtomicRadius(range) => element
                .atomic_radius()
                .map(|n| lerp(n.0, range, 0.0..=1.0))
                .map(|n| get_color(gradient, n)),
            ColormapType::AtomicWeight(range) => Some(f64::from(element.atomic_weight()))
                .map(|n| lerp(n, range, 0.0..=1.0))
                .map(|n| get_color(gradient, n)),
            ColormapType::Year(range) => get_year(element)
                .map(|n| lerp(n, range, 0.0..=1.0))
                .map(|n| get_color(gradient, n)),
        }
    }
}

fn get_atomic_number(element: &mendeleev::Element) -> Option<u32> {
    Some(element.atomic_number())
}

fn get_year(element: &mendeleev::Element) -> Option<u16> {
    match element.year_discovered() {
        mendeleev::YearDiscovered::Ancient => None,
        mendeleev::YearDiscovered::Known(year) => Some(year),
    }
}

fn lerp<T>(value: T, src: &RangeInclusive<T>, dst: RangeInclusive<f64>) -> f64
where
    T: Into<f64> + Clone,
{
    let value = value.into();
    let src = src.start().clone().into()..=src.end().clone().into();
    (value - src.start()) / (src.end() - src.start()) * (dst.end() - dst.start()) + dst.start()
}

fn get_color(gradient: &colorgrad::Gradient, position: f64) -> Color32 {
    let [r, g, b, _a] = gradient.at(position).to_rgba8();
    Color32::from_rgb(r, g, b)
}

fn cpk_to_egui_color(c: Option<mendeleev::Color>) -> Option<Color32> {
    let c = c?;
    Some(Color32::from_rgb(c.r, c.g, c.b))
}

pub struct ElementColorMapper {
    colormap_type: ColormapType,
    available_maps: Vec<ColormapType>,
    current_gradient: usize,
    gradients: Vec<colorgrad::Gradient>,
    texture_handles: HashMap<usize, TextureHandle>,
    show_gradient_options: bool,
}

impl ElementColorMapper {
    pub fn new() -> Self {
        Self {
            colormap_type: ColormapType::Jmol,
            available_maps: vec![
                ColormapType::Cpk,
                ColormapType::Jmol,
                ColormapType::AtomicNumber(1..=118),
                ColormapType::AtomicRadius(20.0..=250.0),
                ColormapType::AtomicWeight(0.0..=300.0),
                ColormapType::Year(1600..=2020),
            ],
            current_gradient: 0,
            gradients: vec![
                colorgrad::turbo(),
                colorgrad::inferno(),
                colorgrad::cubehelix_default(),
                colorgrad::yl_or_rd(),
                colorgrad::rainbow(),
                colorgrad::sinebow(),
            ],
            texture_handles: HashMap::default(),
            show_gradient_options: false,
        }
    }

    pub fn get_element_color(&self, element: &mendeleev::Element) -> Option<Color32> {
        self.gradients
            .get(self.current_gradient)
            .and_then(|g| self.colormap_type.get_element_color(element, g))
    }

    pub fn show(&mut self, ui: &mut Ui) {
        ui.group(|ui| {
            ui.label("Color map");
            ComboBox::from_id_source("periodic_table_coloring")
                .selected_text(self.colormap_type.to_string())
                .show_ui(ui, |ui| {
                    for filter in &self.available_maps {
                        ui.selectable_value(
                            &mut self.colormap_type,
                            filter.clone(),
                            filter.to_string(),
                        );
                    }
                });
            if !matches!(self.colormap_type, ColormapType::Cpk | ColormapType::Jmol) {
                self.show_gradient(ui);
            }
            match &mut self.colormap_type {
                ColormapType::Cpk => {}
                ColormapType::Jmol => {}
                ColormapType::AtomicNumber(range) => Self::edit_range(ui, range, 1..=118),
                ColormapType::AtomicRadius(range) => Self::edit_range(ui, range, 20.0..=250.0),
                ColormapType::AtomicWeight(range) => Self::edit_range(ui, range, 0.0..=300.0),
                ColormapType::Year(range) => Self::edit_range(ui, range, 1600..=2020),
            }
        });
    }

    fn get_or_create_image_texture(
        &mut self,
        ui: &mut Ui,
        gradient_index: usize,
    ) -> Option<&mut TextureHandle> {
        let ctx = ui.ctx();
        self.gradients.get(gradient_index).map(|grad| {
            self.texture_handles.entry(gradient_index).or_insert({
                let pixels: Vec<_> = (0..=255)
                    .map(|i| i as f64 / 255.0)
                    .map(|pos| get_color(grad, pos))
                    .collect();
                let width = pixels.len();
                let height = 1;
                ctx.load_texture(
                    "gradient_display",
                    eframe::epaint::ColorImage {
                        size: [width, height],
                        pixels,
                    },
                    TextureOptions::LINEAR,
                )
            })
        })
    }

    fn show_gradient(&mut self, ui: &mut Ui) {
        let img_size = [
            ui.spacing().interact_size.x * 2.0,
            ui.spacing().interact_size.y,
        ];
        if let Some(texture) = self.get_or_create_image_texture(ui, self.current_gradient) {
            let texture = SizedTexture::new(texture, img_size);
            if ui.add(egui::ImageButton::new(texture)).clicked() {
                self.show_gradient_options ^= true;
            }
        }
        if self.show_gradient_options {
            (0..self.gradients.len()).for_each(|i| {
                if let Some(texture) = self.get_or_create_image_texture(ui, i) {
                    let texture = SizedTexture::new(texture, img_size);
                    if ui.add(egui::ImageButton::new(texture)).clicked() {
                        self.current_gradient = i;
                        self.show_gradient_options = false;
                    }
                }
            });
        }
    }

    fn edit_range<N>(ui: &mut Ui, range: &mut RangeInclusive<N>, default_range: RangeInclusive<N>)
    where
        N: emath::Numeric,
    {
        let (mut start, mut end) = (*range.start(), *range.end());
        ui.horizontal(|ui| {
            ui.add(egui::DragValue::new(&mut start).clamp_range(*default_range.start()..=end));
            ui.add(egui::DragValue::new(&mut end).clamp_range(start..=*default_range.end()));
        });
        *range = start..=end;
    }
}
