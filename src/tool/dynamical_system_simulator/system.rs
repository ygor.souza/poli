use std::cell::RefCell;
use std::collections::HashMap;
use std::time::Duration;

use enum_iterator::all;
use enum_iterator::Sequence;

use rand::prelude::*;
use rand::SeedableRng;

use super::errors::SimulationParametersError;
use super::errors::SystemDefinitionError;
use super::errors::MAX_OUTPUT_STEPS;
use super::errors::MAX_SIMULATION_STEPS;
use super::errors::MAX_VARIABLES;
use super::errors::MAX_VARIABLE_VALUE;
use super::errors::MIN_TIME_STEP;
use super::errors::{SimulationError, VariableDefinitionError, VariableDefinitionErrorKind};

use serde::{Deserialize, Serialize};

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct DynamicalSystem {
    variables: Vec<CustomVariable>,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, Serialize, Deserialize)]
pub struct SimulationParameters {
    start_time: Duration,
    end_time: Duration,
    time_step: Duration,
    noise_seed: u64,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct SimulationDefinition {
    pub system: DynamicalSystem,
    pub parameters: SimulationParameters,
}

pub struct SimulationResults {
    pub parameters: SimulationParameters,
    pub variables: Vec<CustomVariable>,
    pub signals: Vec<Signal>,
}

pub struct Signal {
    pub variable: VariableKind,
    pub values: Vec<f64>,
}

#[derive(PartialEq, Eq, Clone, Copy, Sequence, Debug)]
pub enum PredefinedVariable {
    Time,
    Noise,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct CustomVariable {
    name: String,
    initial_value: f64,
    update_rule: Option<String>,
}

#[derive(Clone, PartialEq, Debug)]
pub enum VariableKind {
    Predefined(PredefinedVariable),
    Custom(CustomVariable),
}

impl PredefinedVariable {
    pub fn name(&self) -> &'static str {
        match self {
            PredefinedVariable::Time => "t",
            PredefinedVariable::Noise => "noise",
        }
    }
}

impl CustomVariable {
    pub fn new(name: &str, initial_value: f64) -> Self {
        Self {
            name: name.to_owned(),
            initial_value,
            update_rule: None,
        }
    }

    pub fn name(&self) -> &str {
        self.name.as_ref()
    }

    pub fn name_mut(&mut self) -> &mut String {
        &mut self.name
    }

    pub fn initial_value_mut(&mut self) -> &mut f64 {
        &mut self.initial_value
    }

    pub fn update_rule(&self) -> Option<&String> {
        self.update_rule.as_ref()
    }

    pub fn set_update_rule(&mut self, update_rule: Option<&str>) {
        self.update_rule = update_rule
            .map(|rule| rule.trim_start().to_owned())
            .filter(|rule| !rule.is_empty());
    }

    pub fn with_update_rule(mut self, update_rule: &str) -> Self {
        self.set_update_rule(Some(update_rule));
        self
    }
}

impl VariableKind {
    pub fn name(&self) -> &str {
        match self {
            VariableKind::Predefined(var) => var.name(),
            VariableKind::Custom(var) => var.name(),
        }
    }
}

impl SimulationParameters {
    pub fn start_time_mut(&mut self) -> &mut Duration {
        &mut self.start_time
    }

    pub fn end_time_mut(&mut self) -> &mut Duration {
        &mut self.end_time
    }

    pub fn time_step_mut(&mut self) -> &mut Duration {
        &mut self.time_step
    }

    pub fn noise_seed_mut(&mut self) -> &mut u64 {
        &mut self.noise_seed
    }
    pub fn validate(&self) -> Result<(), SimulationParametersError> {
        let output_duration = self.end_time.checked_sub(self.start_time);
        match output_duration {
            None => Err(SimulationParametersError::StartAfterEnd),
            Some(output_duration) => {
                if self.time_step < MIN_TIME_STEP {
                    Err(SimulationParametersError::TimeStepTooSmall)
                } else {
                    let total_steps = self.end_time.as_secs_f64() / self.time_step.as_secs_f64();
                    let output_steps = output_duration.as_secs_f64() / self.time_step.as_secs_f64();
                    if total_steps > MAX_SIMULATION_STEPS as f64 {
                        Err(SimulationParametersError::TooManySimulationSteps)
                    } else if output_steps > MAX_OUTPUT_STEPS as f64 {
                        Err(SimulationParametersError::TooManyOutputSteps)
                    } else {
                        Ok(())
                    }
                }
            }
        }
    }
}

impl Default for SimulationParameters {
    fn default() -> Self {
        Self {
            start_time: Duration::ZERO,
            end_time: Duration::from_secs(10),
            time_step: Duration::from_millis(1),
            noise_seed: 0,
        }
    }
}

impl DynamicalSystem {
    pub fn variables(&self) -> &[CustomVariable] {
        self.variables.as_ref()
    }

    pub fn set_variables(&mut self, variables: Vec<CustomVariable>) {
        self.variables = variables;
    }

    pub fn validate(&self) -> Result<(), SystemDefinitionError> {
        let predefined_variable_names: Vec<_> =
            all::<PredefinedVariable>().map(|v| v.name()).collect();
        let custom_variable_names: Vec<_> =
            self.variables.iter().map(|var| var.name.as_str()).collect();
        let all_variable_names: Vec<_> = predefined_variable_names
            .iter()
            .cloned()
            .chain(custom_variable_names.iter().cloned())
            .collect();
        let unavailable_names: Vec<_> = ["pi", "e"]
            .into_iter()
            .chain(predefined_variable_names)
            .collect();
        let name_is_valid = |s: &str| {
            s.starts_with(char::is_alphabetic) && s.chars().all(|c| c.is_alphanumeric() || c == '_')
        };
        if custom_variable_names.len() > MAX_VARIABLES {
            return Err(SystemDefinitionError::TooManyVariables);
        }
        let invalid_name_errors = custom_variable_names
            .iter()
            .filter(|name| !name_is_valid(name))
            .map(|name| VariableDefinitionError {
                name: String::from(*name),
                kind: VariableDefinitionErrorKind::InvalidName,
            });
        let unavailable_name_errors = custom_variable_names
            .iter()
            .filter(|name| unavailable_names.contains(*name))
            .map(|name| VariableDefinitionError {
                name: String::from(*name),
                kind: VariableDefinitionErrorKind::UnavailableName,
            });
        let differential_variable_names: Vec<_> = all_variable_names
            .iter()
            .map(|s| "d".to_owned() + s)
            .collect();
        let variables_and_differentials: Vec<_> = all_variable_names
            .iter()
            .cloned()
            .chain(differential_variable_names.iter().map(|s| s.as_str()))
            .collect();
        let duplicate_name_errors: Vec<_> = variables_and_differentials
            .iter()
            .fold(HashMap::new(), |mut dict, name| {
                dict.entry(name)
                    .and_modify(|count| *count += 1)
                    .or_insert(1);
                dict
            })
            .into_iter()
            .filter(|(_name, count)| *count > 1)
            .map(|(name, _)| VariableDefinitionError {
                name: String::from(*name),
                kind: VariableDefinitionErrorKind::DuplicateName,
            })
            .collect();

        let invalid_expression_errors = self.variables.iter().flat_map(|var| {
            let rule = var.update_rule()?;
            let f = rule
                .parse::<meval::Expr>()
                .and_then(|expr| expr.bindn(&variables_and_differentials));
            match f {
                Ok(_) => None,
                Err(err) => Some((var.name.clone(), err.into()).into()),
            }
        });

        let variable_definition_errors: Vec<_> = invalid_name_errors
            .chain(unavailable_name_errors)
            .chain(duplicate_name_errors)
            .chain(invalid_expression_errors)
            .collect();
        if !variable_definition_errors.is_empty() {
            Err(SystemDefinitionError::InvalidVariableDefinitions(
                variable_definition_errors,
            ))
        } else {
            Ok(())
        }
    }

    #[allow(clippy::type_complexity)]
    pub fn run_simulation(
        &self,
        parameters: &SimulationParameters,
    ) -> Result<SimulationResults, SimulationError> {
        self.validate()?;
        parameters.validate()?;
        let all_variables: Vec<_> = all::<PredefinedVariable>()
            .map(VariableKind::Predefined)
            .chain(self.variables().iter().cloned().map(VariableKind::Custom))
            .collect();
        let all_variable_names: Vec<_> = all_variables.iter().map(|var| var.name()).collect();
        let differential_variable_names: Vec<_> = all_variable_names
            .iter()
            .map(|s| "d".to_owned() + s)
            .collect();
        let variables_and_differentials: Vec<_> = all_variable_names
            .iter()
            .cloned()
            .chain(differential_variable_names.iter().map(|s| s.as_str()))
            .collect();
        let ref_t = RefCell::new(0.0);
        let start_time = parameters.start_time.as_secs_f64();
        let end_time = parameters.end_time.as_secs_f64();
        let dt = parameters.time_step.as_secs_f64();
        let mut rng = SmallRng::seed_from_u64(parameters.noise_seed);
        let noise = RefCell::new(move || rng.gen_range(-1.0..=1.0));
        let noise = || {
            noise
                .try_borrow_mut()
                .map(|mut noise| noise())
                .unwrap_or(f64::NAN)
        };
        let (initial_values, initial_differentials): (Vec<_>, Vec<_>) = all_variables
            .iter()
            .map(|var| match var {
                VariableKind::Predefined(PredefinedVariable::Time) => {
                    (*ref_t.try_borrow().as_deref().unwrap_or(&f64::NAN), dt)
                }
                VariableKind::Predefined(PredefinedVariable::Noise) => (noise(), noise()),
                VariableKind::Custom(var) => (var.initial_value, var.initial_value),
            })
            .unzip();
        let mut current_values = initial_values.clone();
        let mut previous_values: Vec<_> = initial_values
            .iter()
            .zip(initial_differentials)
            .map(|(curr, prev)| curr - prev)
            .collect();
        let update_rules: Vec<_> = all_variables
            .iter()
            .map(|var| -> Box<dyn Fn(&[f64]) -> f64> {
                match var {
                    VariableKind::Predefined(PredefinedVariable::Time) => {
                        Box::new(|_values| match ref_t.try_borrow_mut() {
                            Ok(mut t) => {
                                *t += dt;
                                *t
                            }
                            Err(_) => f64::NAN,
                        })
                    }
                    VariableKind::Predefined(PredefinedVariable::Noise) => {
                        Box::new(|_values| noise())
                    }
                    VariableKind::Custom(var) => {
                        if let Ok(func) = var.update_rule().ok_or(()).and_then(|rule| {
                            rule.parse::<meval::Expr>()
                                .and_then(|expr| expr.bindn(&variables_and_differentials))
                                .map_err(|_| ())
                        }) {
                            Box::new(func)
                        } else {
                            Box::new(|_values| var.initial_value)
                        }
                    }
                }
            })
            .collect();
        let mut time_steps = vec![];
        for _ in 0..MAX_SIMULATION_STEPS {
            if let Ok(t) = ref_t.try_borrow() {
                if *t >= start_time {
                    time_steps.push(current_values.clone());
                }
                if *t >= end_time {
                    break;
                }
            }
            let current_differentials = current_values
                .iter()
                .zip(previous_values.iter())
                .map(|(curr, prev)| curr - prev);
            let values_and_differentials: Vec<_> = current_values
                .iter()
                .cloned()
                .chain(current_differentials)
                .collect();
            previous_values.clone_from(&current_values);
            current_values = update_rules
                .iter()
                .map(|rule| rule(&values_and_differentials))
                .collect();
            current_values
                .iter()
                .all(|v| v.abs() < MAX_VARIABLE_VALUE)
                .then_some(())
                .ok_or(SimulationError::VariableValueTooLarge)?;
        }
        let signals = all_variables
            .iter()
            .cloned()
            .enumerate()
            .map(|(i, var)| {
                let values: Vec<_> = time_steps
                    .iter()
                    .map(|step| *step.get(i).unwrap_or(&f64::NAN))
                    .collect();
                Signal {
                    variable: var,
                    values,
                }
            })
            .collect();
        let results = SimulationResults {
            parameters: *parameters,
            variables: self.variables.clone(),
            signals,
        };
        Ok(results)
    }
}
impl Default for DynamicalSystem {
    fn default() -> Self {
        Self {
            variables: [
                CustomVariable::new("R", 1.0),
                CustomVariable::new("L", 1.0),
                CustomVariable::new("C", 1.0),
                CustomVariable::new("u0", 1.0),
                CustomVariable::new("u", 1.0).with_update_rule("u0 + 0.2 * sin(t) + 0.05 * noise"),
                CustomVariable::new("vc", 0.0).with_update_rule("vc + dt * il/C"),
                CustomVariable::new("il", 0.0).with_update_rule("il + dt * (u - vc - il * R)/L"),
                CustomVariable::new("il_dot", 0.0).with_update_rule("dil/dt"),
            ]
            .into(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn default_parameters_are_valid() {
        assert!(SimulationParameters::default().validate().is_ok());
    }

    #[test]
    fn default_system_is_valid() {
        assert!(DynamicalSystem::default().validate().is_ok());
    }

    #[test]
    fn default_simulation_is_valid() {
        assert!(DynamicalSystem::default()
            .run_simulation(&SimulationParameters::default())
            .is_ok());
    }

    #[test]
    fn system_limits_number_of_variables() {
        let system = DynamicalSystem {
            variables: (0..=MAX_VARIABLES)
                .map(|i| CustomVariable {
                    name: format!("x{i}"),
                    initial_value: 0.0,
                    update_rule: None,
                })
                .collect(),
        };
        assert!(matches!(
            system.validate(),
            Err(SystemDefinitionError::TooManyVariables)
        ));
        assert!(matches!(
            system.run_simulation(&SimulationParameters::default()),
            Err(SimulationError::SystemDefinitionError(
                SystemDefinitionError::TooManyVariables
            ))
        ));
    }

    #[test]
    fn system_rejects_invalid_names() {
        let n_variables = 5;
        let system = DynamicalSystem {
            variables: (0..n_variables)
                .map(|i| CustomVariable {
                    name: match i {
                        0 => "",
                        1 => " ",
                        2 => " a b",
                        3 => "!a",
                        _ => "x#",
                    }
                    .to_owned(),
                    initial_value: 0.0,
                    update_rule: None,
                })
                .collect(),
        };
        assert!(matches!(
            system.validate(),
            Err(SystemDefinitionError::InvalidVariableDefinitions(err) ) if err.len() == n_variables
        ));
        assert!(matches!(
            system.run_simulation(&SimulationParameters::default()),
            Err(SimulationError::SystemDefinitionError(
                SystemDefinitionError::InvalidVariableDefinitions(_)
            ))
        ));
    }

    #[test]
    fn system_rejects_invalid_rules() {
        let n_variables = 5;
        let system = DynamicalSystem {
            variables: (0..n_variables)
                .map(|i| CustomVariable {
                    name: format!("x{i}"),
                    initial_value: 0.0,
                    update_rule: match i {
                        0 => " -",
                        1 => "* x1",
                        2 => "% x1",
                        3 => "(x1",
                        _ => "y1 + y2",
                    }
                    .to_owned()
                    .into(),
                })
                .collect(),
        };

        assert!(matches!(
            system.validate(),
            Err(SystemDefinitionError::InvalidVariableDefinitions(err)) if err.len() == n_variables
        ));
        assert!(matches!(
            system.run_simulation(&SimulationParameters::default()),
            Err(SimulationError::SystemDefinitionError(
                SystemDefinitionError::InvalidVariableDefinitions(_)
            ))
        ));
    }

    #[test]
    fn system_rejects_small_time_step() {
        let system = DynamicalSystem::default();
        let parameters = SimulationParameters {
            start_time: Duration::ZERO,
            end_time: Duration::from_secs(1),
            time_step: Duration::ZERO,
            noise_seed: 0,
        };

        assert!(matches!(
            parameters.validate(),
            Err(SimulationParametersError::TimeStepTooSmall)
        ));
        assert!(matches!(
            system.run_simulation(&parameters),
            Err(SimulationError::SimulationParametersError(
                SimulationParametersError::TimeStepTooSmall
            ))
        ));
    }

    #[test]
    fn simulation_stops_if_value_is_too_large() {
        let system = DynamicalSystem {
            variables: vec![CustomVariable {
                name: "x".to_owned(),
                initial_value: 1.0,
                update_rule: Some("x*2".to_owned()),
            }],
        };
        let parameters = SimulationParameters::default();

        assert!(matches!(
            system.run_simulation(&parameters),
            Err(SimulationError::VariableValueTooLarge)
        ));
    }
}
