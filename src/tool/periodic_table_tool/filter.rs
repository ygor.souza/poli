use std::{fmt::Display, ops::RangeInclusive};

use eframe::egui::{self, ComboBox, TextEdit, Ui};

#[derive(PartialEq, Clone)]
pub enum FilterType {
    Name(String),
    AtomicNumber(RangeInclusive<u32>),
    AtomicRadius(RangeInclusive<f64>),
    AtomicWeight(RangeInclusive<f64>),
    Year(Option<RangeInclusive<u16>>),
}

impl Display for FilterType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            FilterType::Name(_) => "Name",
            FilterType::AtomicNumber(_) => "Atomic number",
            FilterType::AtomicRadius(_) => "Atomic radius",
            FilterType::AtomicWeight(_) => "Atomic weight",
            FilterType::Year(_) => "Year discovered",
        };
        f.write_str(s)?;
        Ok(())
    }
}

impl FilterType {
    pub fn test_element(&self, element: &mendeleev::Element) -> bool {
        match &self {
            FilterType::Name(pattern) => element
                .name()
                .to_lowercase()
                .contains(&pattern.to_lowercase()),
            FilterType::AtomicNumber(range) => range.contains(&element.atomic_number()),
            FilterType::AtomicRadius(range) => element
                .atomic_radius()
                .map(|r| range.contains(&r.0))
                .unwrap_or(false),
            FilterType::AtomicWeight(range) => Some(f64::from(element.atomic_weight()))
                .map(|m: f64| range.contains(&m))
                .unwrap_or(false),
            FilterType::Year(None) => {
                matches!(
                    element.year_discovered(),
                    mendeleev::YearDiscovered::Ancient
                )
            }
            FilterType::Year(Some(range)) => {
                if let mendeleev::YearDiscovered::Known(year) = element.year_discovered() {
                    range.contains(&year)
                } else {
                    false
                }
            }
        }
    }
}

pub struct ElementFilter {
    id: u64,
    is_enabled: bool,
    is_negated: bool,
    filter_type: FilterType,
    available_filters: Vec<FilterType>,
}

impl ElementFilter {
    pub fn new(id: u64) -> Self {
        Self {
            id,
            is_enabled: true,
            is_negated: false,
            filter_type: FilterType::Name("ium".into()),
            available_filters: vec![
                FilterType::Name(String::new()),
                FilterType::AtomicNumber(1..=118),
                FilterType::AtomicRadius(20.0..=250.0),
                FilterType::AtomicWeight(0.0..=300.0),
                FilterType::Year(None),
            ],
        }
    }

    pub fn test_element(&self, element: &mendeleev::Element) -> bool {
        !self.is_enabled || self.is_negated ^ self.filter_type.test_element(element)
    }

    pub fn show_and_check_if_deleted(&mut self, ui: &mut Ui) -> bool {
        let mut deleted = false;
        ui.vertical(|ui| {
            ui.horizontal(|ui| {
                ui.checkbox(&mut self.is_enabled, "Enabled");
                deleted = ui.button("❌").clicked();
            });
            ComboBox::from_id_source(self.id)
                .selected_text(self.filter_type.to_string())
                .show_ui(ui, |ui| {
                    for filter in &self.available_filters {
                        ui.selectable_value(
                            &mut self.filter_type,
                            filter.clone(),
                            filter.to_string(),
                        );
                    }
                });
            match &mut self.filter_type {
                FilterType::Name(pattern) => {
                    ui.add_sized([150.0, 20.0], TextEdit::singleline(pattern));
                }
                FilterType::AtomicNumber(range) => {
                    let (mut start, mut end) = (*range.start(), *range.end());
                    ui.horizontal(|ui| {
                        ui.add(egui::DragValue::new(&mut start).clamp_range(1..=end));
                        ui.add(egui::DragValue::new(&mut end).clamp_range(start..=118));
                    });
                    *range = start..=end;
                }
                FilterType::AtomicRadius(range) => {
                    let (mut start, mut end) = (*range.start(), *range.end());
                    ui.horizontal(|ui| {
                        ui.add(egui::DragValue::new(&mut start).clamp_range(20.0..=end));
                        ui.add(egui::DragValue::new(&mut end).clamp_range(start..=250.0));
                    });
                    *range = start..=end;
                }
                FilterType::AtomicWeight(range) => {
                    let (mut start, mut end) = (*range.start(), *range.end());
                    ui.horizontal(|ui| {
                        ui.add(egui::DragValue::new(&mut start).clamp_range(0.0..=end));
                        ui.add(egui::DragValue::new(&mut end).clamp_range(start..=300.0));
                    });
                    *range = start..=end;
                }
                FilterType::Year(opt_range) => {
                    let mut ancient = opt_range.is_none();
                    ui.checkbox(&mut ancient, "Ancient");
                    if ancient {
                        *opt_range = None;
                    } else {
                        let (mut start, mut end) = if let Some(range) = opt_range {
                            (*range.start(), *range.end())
                        } else {
                            (1600, 2020)
                        };
                        ui.horizontal(|ui| {
                            ui.add(egui::DragValue::new(&mut start).clamp_range(1600..=end));
                            ui.add(egui::DragValue::new(&mut end).clamp_range(start..=2020));
                        });
                        *opt_range = Some(start..=end);
                    }
                }
            }
        });
        deleted
    }
}
