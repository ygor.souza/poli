#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release

mod logging;
mod tool;

use std::{
    collections::{BTreeSet, VecDeque},
    sync::mpsc::{self, Receiver},
};

use eframe::{
    egui::{self, global_dark_light_mode_switch, Button, Layout, RichText},
    emath::Align,
    epaint::Color32,
};

use egui_extras::{Column, TableBuilder};

use logging::LogEntry;
use tool::{all_tools, Tool};

fn main() -> Result<(), eframe::Error> {
    log::set_logger(&logging::LOGGER).expect("Failed to set logger");
    log::set_max_level(log::LevelFilter::Trace);
    let options = eframe::NativeOptions::default();
    eframe::run_native(
        env!("CARGO_PKG_NAME"),
        options,
        Box::new(|_cc| Box::<Application>::default()),
    )
}

struct ToolWindow {
    tool: Box<dyn Tool>,
    window_open: bool,
}

struct Application {
    tools: Vec<ToolWindow>,
    selected_index: Option<usize>,
    show_about_window: bool,
    show_logs_window: bool,
    log_receiver: Receiver<LogEntry>,
    logs: VecDeque<LogEntry>,
    log_filter: BTreeSet<log::Level>,
}

impl Default for Application {
    fn default() -> Self {
        let tools = all_tools()
            .into_iter()
            .map(|tool| ToolWindow {
                tool,
                window_open: false,
            })
            .collect();
        let (log_sender, log_receiver) = mpsc::channel();
        *logging::SENDER.lock().expect("Failed to initialize logger") = Some(log_sender);
        let log_filter = log::Level::iter().collect();

        Self {
            tools,
            selected_index: None,
            show_about_window: false,
            show_logs_window: false,
            log_receiver,
            logs: Default::default(),
            log_filter,
        }
    }
}

impl Application {
    fn show_about_window(&mut self, ui: &mut egui::Ui) {
        egui::Window::new("About")
            .open(&mut self.show_about_window)
            .show(ui.ctx(), |ui| {
                ui.heading(format!(
                    "{} - {}",
                    env!("CARGO_PKG_NAME"),
                    env!("CARGO_PKG_VERSION")
                ));
                ui.label(env!("CARGO_PKG_DESCRIPTION"));
                ui.label(format!("Authors: {}", env!("CARGO_PKG_AUTHORS")));
                ui.label(format!("⚖ {}", env!("CARGO_PKG_LICENSE")));
                ui.add(egui::Hyperlink::from_label_and_url(
                    "Repository",
                    env!("CARGO_PKG_REPOSITORY"),
                ));
            });
    }

    fn show_logs_window(&mut self, ui: &mut egui::Ui) {
        egui::Window::new("Logs")
            .open(&mut self.show_logs_window)
            .show(ui.ctx(), |ui| {
                while let Ok(s) = self.log_receiver.try_recv() {
                    self.logs.push_front(s);
                }
                self.logs.truncate(10000);
                ui.collapsing("Filter", |ui| {
                    self.log_filter = log::Level::iter()
                        .filter(|level| {
                            let mut enabled = self.log_filter.contains(level);
                            ui.checkbox(&mut enabled, level.as_str());
                            enabled
                        })
                        .collect();
                });
                let total_logs = self.logs.len();
                let filtered_log_indices: Vec<_> = (0..total_logs)
                    .filter_map(|i| {
                        self.log_filter
                            .contains(&self.logs.get(i)?.level)
                            .then_some(i)
                    })
                    .collect();
                let row_height = ui.text_style_height(&egui::TextStyle::Body);
                let total_rows = filtered_log_indices.len();
                egui::ScrollArea::vertical().max_height(500.0).show_rows(
                    ui,
                    row_height,
                    total_rows,
                    |ui, row_range| {
                        ui.allocate_space([300.0, 0.0].into());
                        let logs_to_display = row_range.filter_map(|i| {
                            filtered_log_indices.get(i).and_then(|i| self.logs.get(*i))
                        });
                        for entry in logs_to_display {
                            ui.horizontal(|ui| {
                                let level = entry.level;
                                let level_icon = match level {
                                    log::Level::Error => "❎",
                                    log::Level::Warn => "⚠",
                                    log::Level::Info => "ℹ",
                                    log::Level::Debug => "🐛",
                                    log::Level::Trace => "📊",
                                };
                                let level_color = match level {
                                    log::Level::Error => Color32::RED,
                                    log::Level::Warn => Color32::YELLOW,
                                    log::Level::Info => Color32::BLUE,
                                    log::Level::Debug => Color32::DARK_GREEN,
                                    log::Level::Trace => Color32::GRAY,
                                };
                                let message = entry.message.as_str();
                                ui.monospace(RichText::new(level_icon).color(level_color))
                                    .on_hover_text(level.to_string());
                                ui.monospace(entry.timestamp.format("%H:%M:%S.%3f").to_string())
                                    .on_hover_text(entry.timestamp.to_rfc3339());
                                ui.monospace(message).on_hover_text(message);
                            });
                        }
                    },
                );
            });
    }
}

impl eframe::App for Application {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::SidePanel::left("tools_menu").show(ctx, |ui| {
            let table = TableBuilder::new(ui)
                .cell_layout(egui::Layout::left_to_right(egui::Align::Center))
                .column(Column::initial(120.0))
                .column(Column::initial(20.0))
                .resizable(false);

            table.body(|mut body| {
                for (index, tool_window) in self.tools.iter_mut().enumerate() {
                    let name = RichText::new(tool_window.tool.name());
                    let selected = self.selected_index == Some(index);
                    body.row(20.0, |mut row| {
                        row.col(|ui| {
                            let button = if selected {
                                let highlight = ui.ctx().style().visuals.extreme_bg_color;
                                Button::new(name).fill(highlight)
                            } else {
                                Button::new(name)
                            };
                            if ui.add(button).clicked() {
                                if selected {
                                    self.selected_index = None;
                                } else {
                                    self.selected_index = Some(index);
                                }
                            }
                        });
                        row.col(|ui| {
                            if !tool_window.window_open
                                && ui.button("⮫").on_hover_text("Open window").clicked()
                            {
                                tool_window.window_open = true;
                            } else if tool_window.window_open
                                && ui.button("❌").on_hover_text("Close window").clicked()
                            {
                                tool_window.window_open = false;
                            };
                        });
                    });
                }
            });
            for tool_window in self.tools.iter_mut() {
                egui::Window::new(tool_window.tool.name())
                    .open(&mut tool_window.window_open)
                    .show(ui.ctx(), |ui| {
                        tool_window.tool.view(ui);
                    });
            }
            ui.with_layout(Layout::bottom_up(Align::LEFT), |ui| {
                ui.horizontal(|ui| {
                    global_dark_light_mode_switch(ui);
                    if ui.button("ℹ").on_hover_text("About").clicked() {
                        self.show_about_window ^= true;
                    }
                    if ui.button("📄").on_hover_text("Logs").clicked() {
                        self.show_logs_window ^= true;
                    }
                    self.show_about_window(ui);
                    self.show_logs_window(ui);
                });
            });
        });
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.vertical(|ui| {
                if let Some(t) = self.selected_index.and_then(|i| self.tools.get_mut(i)) {
                    if !t.window_open {
                        t.tool.view(ui);
                    }
                }
            });
        });
    }
}
