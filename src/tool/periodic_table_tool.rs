use self::colormap::ElementColorMapper;

use super::Tool;
use eframe::{
    egui::{self, Frame, Label, RichText, ScrollArea},
    epaint::Color32,
};

mod colormap;
mod filter;

const ELEMENT_SIZE: [f32; 2] = [35.0, 35.0];

pub struct PeriodicTableTool {
    last_id: u64,
    filters: Vec<filter::ElementFilter>,
    colormap: ElementColorMapper,
}

impl PeriodicTableTool {
    pub fn new() -> Self {
        Self {
            last_id: 0,
            filters: Vec::new(),
            colormap: ElementColorMapper::new(),
        }
    }
}

impl Default for PeriodicTableTool {
    fn default() -> Self {
        Self::new()
    }
}

impl PeriodicTableTool {
    fn view_element(&self, ui: &mut egui::Ui, element: &mendeleev::Element) {
        let is_enabled = self.filters.iter().all(|f| f.test_element(element));
        let background = if is_enabled {
            self.colormap
                .get_element_color(element)
                .unwrap_or(Color32::from_rgb(200, 200, 200))
        } else {
            Color32::from_rgb(30, 30, 30)
        };
        let foreground = if !is_enabled {
            Color32::from_rgb(120, 120, 120)
        } else if (background.r() as u32 + background.g() as u32 + background.b() as u32) < 150 {
            Color32::WHITE
        } else {
            Color32::BLACK
        };
        Frame::none()
            .fill(background)
            .stroke(eframe::epaint::Stroke::new(1.0, Color32::BLACK))
            .show(ui, |ui| {
                ui.add_sized(
                    ELEMENT_SIZE,
                    Label::new(RichText::from(element.symbol()).color(foreground).heading()),
                )
                .on_hover_ui(|ui| {
                    ui.heading(element.name());
                    ui.label(format!("Atomic number: {}", element.atomic_number()));
                    ui.label(format!("Atomic weight: {}", element.atomic_weight()));
                    element
                        .atomic_radius()
                        .map(|r| ui.label(format!("Atomic radius: {r}")));
                    let year = match &element.year_discovered() {
                        mendeleev::YearDiscovered::Ancient => "Discovery date unknown".to_string(),
                        mendeleev::YearDiscovered::Known(y) => format!("Discovered in {y}"),
                    };
                    ui.label(year);
                })
            });
    }

    fn skip_element(&self, ui: &mut egui::Ui) {
        ui.allocate_space(ELEMENT_SIZE.into());
    }

    fn view_table(&self, ui: &mut egui::Ui) {
        ui.spacing_mut().item_spacing = [4.0, 4.0].into();
        for period in 1..=mendeleev::N_PERIODS {
            ui.horizontal(|ui| {
                for group in mendeleev::Group::list() {
                    if let Some(element) = mendeleev::Element::list()
                        .iter()
                        .find(|el| el.period() == period && el.group() == Some(*group))
                    {
                        self.view_element(ui, element);
                    } else {
                        self.skip_element(ui);
                    }
                }
            });
        }
        ui.add_space(20.0);
        for period in 6..=7 {
            ui.horizontal(|ui| {
                for _ in 0..2 {
                    self.skip_element(ui);
                }
                for element in mendeleev::Element::list()
                    .iter()
                    .filter(|el| el.period() == period && el.group().is_none())
                {
                    self.view_element(ui, element)
                }
            });
        }
    }
}

impl Tool for PeriodicTableTool {
    fn name(&self) -> &str {
        "Periodic Table"
    }

    fn view(&mut self, ui: &mut egui::Ui) {
        ui.heading("Periodic table");
        ui.horizontal(|ui| {
            ui.vertical(|ui| {
                self.colormap.show(ui);
                ui.horizontal(|ui| {
                    ui.label("Filters");
                    if ui.button("➕").clicked() {
                        self.filters.push(filter::ElementFilter::new(self.last_id));
                        self.last_id += 1;
                    }
                });
                self.filters
                    .retain_mut(|f| !f.show_and_check_if_deleted(ui))
            });
            ui.vertical(|ui| {
                ScrollArea::horizontal().show(ui, |ui| self.view_table(ui));
            });
        });
    }
}
