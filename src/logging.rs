use std::{
    sync::{mpsc::Sender, Mutex},
    time::SystemTime,
};

use chrono::{DateTime, Local};
use log::{Level, Metadata, Record};

pub(crate) static LOGGER: UiLogger = UiLogger {};
pub(crate) static SENDER: Mutex<Option<Sender<LogEntry>>> = Mutex::new(None);
static THIS_CRATE: &str = env!("CARGO_CRATE_NAME");

pub(crate) struct UiLogger;
pub(crate) struct LogEntry {
    pub timestamp: DateTime<Local>,
    pub level: Level,
    pub message: String,
}

impl log::Log for UiLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata
            .target()
            .split_once("::")
            .map(|(start, _)| start == THIS_CRATE)
            .unwrap_or(false)
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            if let Ok(sender) = SENDER.lock().as_ref() {
                let entry = LogEntry {
                    timestamp: DateTime::from(SystemTime::now()),
                    level: record.level(),
                    message: record.args().to_string(),
                };
                sender.as_ref().map(|s| s.send(entry));
            }
        }
    }

    fn flush(&self) {}
}
